/*
1. Create a new user with the username "rentaluser" and the password "rentalpassword". Give the user the ability to connect to the database but no other permissions.
*/

CREATE USER rentaluser WITH PASSWORD 'rentalpassword';
REVOKE ALL PRIVILEGES ON DATABASE dvdrental FROM rentaluser;

GRANT CONNECT ON DATABASE dvdrental TO rentaluser;

/*
2. Grant "rentaluser" SELECT permission for the "customer" table. Сheck to make sure this permission works correctly—write a SQL query to select all customers.
*/
GRANT SELECT ON customer TO rentaluser;
SELECT * FROM customer;
/*
3. Create a new user group called "rental" and add "rentaluser" to the group. 
*/

CREATE ROLE rental;
GRANT rental TO rentaluser;

/*
4. Grant the "rental" group INSERT and UPDATE permissions for the "rental" table. Insert a new row and update one existing row in the "rental" table under that role.  
*/

GRANT INSERT, UPDATE ON rental TO rental;
INSERT INTO rental (/* column_names */) VALUES (/* values */);

-- Update an existing row in the "rental" table
UPDATE rental SET /* column_name */ = /* new_value */ WHERE /* condition */;

/*
5. Revoke the "rental" group's INSERT permission for the "rental" table. Try to insert new rows into the "rental" table make sure this action is denied.
*/

REVOKE INSERT ON rental FROM rental;

INSERT INTO rental (/* values */);

SELECT c.customer_id, c.first_name, c.last_name
FROM customer c
JOIN payment p ON c.customer_id = p.customer_id
JOIN rental r ON c.customer_id = r.customer_id
GROUP BY c.customer_id
HAVING COUNT(p.payment_id) > 0 AND COUNT(r.rental_id) > 0
LIMIT 1;

/*
6. Create a personalized role for any customer already existing in the dvd_rental database. The name of the role name must be client_{first_name}_{last_name} (omit curly brackets). The customer's payment and rental history must not be empty. Configure that role so that the customer can only access their own data in the "rental" and "payment" tables. Write a query to make sure this user sees only their own data. 
*/

CREATE ROLE client_john_doe;

GRANT USAGE ON SCHEMA public TO client_john_doe;
GRANT SELECT ON payment, rental TO client_john_doe;

ALTER TABLE payment ENABLE ROW LEVEL SECURITY;
ALTER TABLE rental ENABLE ROW LEVEL SECURITY;

CREATE POLICY select_payment ON payment FOR SELECT TO client_john_doe USING (customer_id = (SELECT customer_id FROM customer WHERE first_name = 'John' AND last_name = 'Doe'));
CREATE POLICY select_rental ON rental FOR SELECT TO client_john_doe USING (customer_id = (SELECT customer_id FROM customer WHERE first_name = 'John' AND last_name = 'Doe'));

SET ROLE client_john_doe;
SELECT * FROM payment; -- Test query for payment table
SELECT * FROM rental;  -- Test query for rental table
RESET ROLE;